/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mateusz Aniolek
 */
@Entity
@Table(name = "SCHEDULEDHOURS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Scheduledhours.findAll", query = "SELECT s FROM Scheduledhours s"),
    @NamedQuery(name = "Scheduledhours.findById", query = "SELECT s FROM Scheduledhours s WHERE s.id = :id"),
    @NamedQuery(name = "Scheduledhours.findByUserid", query = "SELECT s FROM Scheduledhours s WHERE s.userid = :userid"),
    @NamedQuery(name = "Scheduledhours.findByWeekday", query = "SELECT s FROM Scheduledhours s WHERE s.weekday = :weekday"),
    @NamedQuery(name = "Scheduledhours.findByStarthour", query = "SELECT s FROM Scheduledhours s WHERE s.starthour = :starthour"),
    @NamedQuery(name = "Scheduledhours.findByEndhour", query = "SELECT s FROM Scheduledhours s WHERE s.endhour = :endhour")})
public class Scheduledhours implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
//    @Basic(optional = false)
//    @NotNull
    @Column(name = "ID", insertable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USERID")
    private int userid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "WEEKDAY")
    private String weekday;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STARTHOUR")
    @Temporal(TemporalType.TIME)
    private Date starthour;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ENDHOUR")
    @Temporal(TemporalType.TIME)
    private Date endhour;

    /**
     *
     */
    public Scheduledhours() {
    }

    /**
     *
     * @param id
     */
    public Scheduledhours(Integer id) {
        this.id = id;
    }

    /**
     *
     * @param id
     * @param userid
     * @param weekday
     * @param starthour
     * @param endhour
     */
    public Scheduledhours(Integer id, int userid, String weekday, Date starthour, Date endhour) {
        this.id = id;
        this.userid = userid;
        this.weekday = weekday;
        this.starthour = starthour;
        this.endhour = endhour;
    }

    /**
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public int getUserid() {
        return userid;
    }

    /**
     *
     * @param userid
     */
    public void setUserid(int userid) {
        this.userid = userid;
    }

    /**
     *
     * @return
     */
    public String getWeekday() {
        return weekday;
    }

    /**
     *
     * @param weekday
     */
    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    /**
     *
     * @return
     */
    public Date getStarthour() {
        return starthour;
    }

    /**
     *
     * @param starthour
     */
    public void setStarthour(Date starthour) {
        this.starthour = starthour;
    }

    /**
     *
     * @return
     */
    public Date getEndhour() {
        return endhour;
    }

    /**
     *
     * @param endhour
     */
    public void setEndhour(Date endhour) {
        this.endhour = endhour;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Scheduledhours)) {
            return false;
        }
        Scheduledhours other = (Scheduledhours) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "entities.Scheduledhours[ id=" + id + " ]";
    }

}
