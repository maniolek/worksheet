/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mateusz Aniolek
 */
@Entity
@Table(name = "WORKHOURS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Workhours.findAll", query = "SELECT w FROM Workhours w"),
    @NamedQuery(name = "Workhours.findById", query = "SELECT w FROM Workhours w WHERE w.id = :id"),
    @NamedQuery(name = "Workhours.findByUserid", query = "SELECT w FROM Workhours w WHERE w.userid = :userid"),
    @NamedQuery(name = "Workhours.findByStarteventid", query = "SELECT w FROM Workhours w WHERE w.starteventid = :starteventid"),
    @NamedQuery(name = "Workhours.findByEndeventid", query = "SELECT w FROM Workhours w WHERE w.endeventid = :endeventid")})
public class Workhours implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
//    @Basic(optional = false)
//    @NotNull
    @Column(name = "ID", insertable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USERID")
    private int userid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STARTEVENTID")
    private int starteventid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ENDEVENTID")
    private int endeventid;

    /**
     *
     */
    public Workhours() {
    }

    /**
     *
     * @param id
     */
    public Workhours(Integer id) {
        this.id = id;
    }

    /**
     *
     * @param id
     * @param userid
     * @param starteventid
     * @param endeventid
     */
    public Workhours(Integer id, int userid, int starteventid, int endeventid) {
        this.id = id;
        this.userid = userid;
        this.starteventid = starteventid;
        this.endeventid = endeventid;
    }

    /**
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public int getUserid() {
        return userid;
    }

    /**
     *
     * @param userid
     */
    public void setUserid(int userid) {
        this.userid = userid;
    }

    /**
     *
     * @return
     */
    public int getStarteventid() {
        return starteventid;
    }

    /**
     *
     * @param starteventid
     */
    public void setStarteventid(int starteventid) {
        this.starteventid = starteventid;
    }

    /**
     *
     * @return
     */
    public int getEndeventid() {
        return endeventid;
    }

    /**
     *
     * @param endeventid
     */
    public void setEndeventid(int endeventid) {
        this.endeventid = endeventid;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Workhours)) {
            return false;
        }
        Workhours other = (Workhours) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "entities.Workhours[ id=" + id + " ]";
    }

}
