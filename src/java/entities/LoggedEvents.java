/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mateusz Aniolek
 */
@Entity
@Table(name = "LOGGEDEVENTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LoggedEvents.findAll", query = "SELECT l FROM LoggedEvents l"),
    @NamedQuery(name = "LoggedEvents.findById", query = "SELECT l FROM LoggedEvents l WHERE l.id = :id"),
    @NamedQuery(name = "LoggedEvents.findByEventtype", query = "SELECT l FROM LoggedEvents l WHERE l.eventtype = :eventtype"),
    @NamedQuery(name = "LoggedEvents.findByEventtimestamp", query = "SELECT l FROM LoggedEvents l WHERE l.eventtimestamp = :eventtimestamp")})
public class LoggedEvents implements Serializable {
    
    /**
     *
     */
    public static final String EVENT_IN = "In";

    /**
     *
     */
    public static final String EVENT_OUT = "Out";
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "EVENTTYPE")
    private String eventtype;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVENTTIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventtimestamp;

    /**
     *
     */
    public LoggedEvents() {
    }

    /**
     *
     * @param id
     */
    public LoggedEvents(Integer id) {
        this.id = id;
    }

    /**
     *
     * @param id
     * @param eventtype
     * @param eventtimestamp
     */
    public LoggedEvents(Integer id, String eventtype, Date eventtimestamp) {
        this.id = id;
        this.eventtype = eventtype;
        this.eventtimestamp = eventtimestamp;
    }

    /**
     *
     * @return
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getEventtype() {
        return eventtype;
    }

    /**
     *
     * @param eventtype
     */
    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    /**
     *
     * @return
     */
    public Date getEventtimestamp() {
        return eventtimestamp;
    }

    /**
     *
     * @param eventtimestamp
     */
    public void setEventtimestamp(Date eventtimestamp) {
        this.eventtimestamp = eventtimestamp;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     *
     * @param object
     * @return
     */
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoggedEvents)) {
            return false;
        }
        LoggedEvents other = (LoggedEvents) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "entities.LoggedEvents[ id=" + id + " ]";
    }

}
