/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.beans;

import entities.Scheduledhours;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Mateusz Aniolek
 */
@Stateless
public class ScheduledhoursFacade extends AbstractFacade<Scheduledhours> {
    @PersistenceContext(unitName = "ZTI_ProjektPU")
    private EntityManager em;

    /**
     *
     * @return
     */
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     *
     */
    public ScheduledhoursFacade() {
        super(Scheduledhours.class);
    }

}
