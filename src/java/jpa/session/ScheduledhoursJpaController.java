/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import entities.Scheduledhours;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import jpa.session.exceptions.NonexistentEntityException;
import jpa.session.exceptions.PreexistingEntityException;
import jpa.session.exceptions.RollbackFailureException;

/**
 *
 * @author Mateusz Aniolek
 */
public class ScheduledhoursJpaController implements Serializable {

    /**
     *
     * @param utx
     * @param emf
     */
    public ScheduledhoursJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    /**
     *
     * @return
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     *
     * @param scheduledhours
     * @throws PreexistingEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(Scheduledhours scheduledhours) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(scheduledhours);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findScheduledhours(scheduledhours.getId()) != null) {
                throw new PreexistingEntityException("Scheduledhours " + scheduledhours + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     *
     * @param scheduledhours
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void edit(Scheduledhours scheduledhours) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            scheduledhours = em.merge(scheduledhours);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = scheduledhours.getId();
                if (findScheduledhours(id) == null) {
                    throw new NonexistentEntityException("The scheduledhours with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     *
     * @param id
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Scheduledhours scheduledhours;
            try {
                scheduledhours = em.getReference(Scheduledhours.class, id);
                scheduledhours.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The scheduledhours with id " + id + " no longer exists.", enfe);
            }
            em.remove(scheduledhours);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     *
     * @return
     */
    public List<Scheduledhours> findScheduledhoursEntities() {
        return findScheduledhoursEntities(true, -1, -1);
    }

    /**
     *
     * @param maxResults
     * @param firstResult
     * @return
     */
    public List<Scheduledhours> findScheduledhoursEntities(int maxResults, int firstResult) {
        return findScheduledhoursEntities(false, maxResults, firstResult);
    }

    private List<Scheduledhours> findScheduledhoursEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Scheduledhours.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    /**
     *
     * @param id
     * @return
     */
    public Scheduledhours findScheduledhours(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Scheduledhours.class, id);
        } finally {
            em.close();
        }
    }

    /**
     *
     * @return
     */
    public int getScheduledhoursCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Scheduledhours> rt = cq.from(Scheduledhours.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
