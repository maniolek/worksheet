/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jpa.session;

import entities.LoggedEvents;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import jpa.session.exceptions.NonexistentEntityException;
import jpa.session.exceptions.PreexistingEntityException;
import jpa.session.exceptions.RollbackFailureException;

/**
 *
 * @author Mateusz Aniolek
 */
public class LoggedEventsJpaController implements Serializable {

    /**
     *
     * @param utx
     * @param emf
     */
    public LoggedEventsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    /**
     *
     * @return
     */
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     *
     * @param loggedEvents
     * @throws PreexistingEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(LoggedEvents loggedEvents) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(loggedEvents);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findLoggedEvents(loggedEvents.getId()) != null) {
                throw new PreexistingEntityException("LoggedEvents " + loggedEvents + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     *
     * @param loggedEvents
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void edit(LoggedEvents loggedEvents) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            loggedEvents = em.merge(loggedEvents);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = loggedEvents.getId();
                if (findLoggedEvents(id) == null) {
                    throw new NonexistentEntityException("The loggedEvents with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     *
     * @param id
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            LoggedEvents loggedEvents;
            try {
                loggedEvents = em.getReference(LoggedEvents.class, id);
                loggedEvents.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The loggedEvents with id " + id + " no longer exists.", enfe);
            }
            em.remove(loggedEvents);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     *
     * @return
     */
    public List<LoggedEvents> findLoggedEventsEntities() {
        return findLoggedEventsEntities(true, -1, -1);
    }

    /**
     *
     * @param maxResults
     * @param firstResult
     * @return
     */
    public List<LoggedEvents> findLoggedEventsEntities(int maxResults, int firstResult) {
        return findLoggedEventsEntities(false, maxResults, firstResult);
    }

    private List<LoggedEvents> findLoggedEventsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LoggedEvents.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    /**
     *
     * @param id
     * @return
     */
    public LoggedEvents findLoggedEvents(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LoggedEvents.class, id);
        } finally {
            em.close();
        }
    }

    /**
     *
     * @return
     */
    public int getLoggedEventsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LoggedEvents> rt = cq.from(LoggedEvents.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
