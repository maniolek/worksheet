/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Wojciech Rutkowski
 */
@Stateless
@ManagedBean(name = "loginBean")
public class LoginBean {

    private String login;
    private String password;

    /**
     *
     * @return
     */
    public String getLogin() {
        return login;
    }

    /**
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     *
     * @return
     */
    public String doLogin() {
                
        FacesContext facesContext = FacesContext.getCurrentInstance();
        AuthBean neededBean = (AuthBean) facesContext.getApplication()
            .createValueBinding("#{authBean}").getValue(facesContext);
        
        neededBean.setScope("Admin");
        neededBean.setAuth(true);
        neededBean.setLogin(this.login);
        
        return "index.xhtml?faces-redirect=true";
    }

    /**
     *
     */
    public void doLogout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) 
            context.getExternalContext().getRequest();
        try {
          request.logout();
        } catch (ServletException e) {
          context.addMessage(null, new FacesMessage("Logout failed."));
        }
    }
    
}
