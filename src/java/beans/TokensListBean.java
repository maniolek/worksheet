/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.List;
import java.util.ListIterator;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


/**
 * @author Wojciech Rutkowski
 */
@Stateless
@ManagedBean (name = "tokensList")
@RequestScoped
public class TokensListBean {

    private String tag;

    /**
     *
     * @return
     */
    public String getTag() {
        return tag;
    }

    /**
     *
     * @param tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }
    
    private List<entities.Tokens> tokens;
    
    @EJB
    private jpa.beans.TokensFacade ejbFacade;
    
    /**
     *
     */
    @PostConstruct
    public void init() {
        tokens = ejbFacade.findAll();
    }

    /**
     *
     * @return
     */
    public List<entities.Tokens> getTokens() {
        return tokens;
    }

    /**
     *
     * @param tokens
     */
    public void setTokens(List<entities.Tokens> tokens) {
        this.tokens = tokens;
    }
    
    /**
     *
     * @param id
     * @return
     */
    public String getToken(Integer id) {
        ListIterator<entities.Tokens> li = tokens.listIterator();
        entities.Tokens user;
        while(li.hasNext()) {
            user = li.next();
            if(user.getId().equals(id)) {
                return user.getTag();
            }
        }
        return "-";
        
    }
}
