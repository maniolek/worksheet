/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.Users;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Mateusz Aniolek
 */
@Stateless
@ManagedBean (name = "workhoursList")
@RequestScoped
public class WorkhoursListBean {

    private String user;
    
    private List<entities.Workhours> workhours;
    
    @EJB
    private jpa.beans.WorkhoursFacade ejbFacade;
    
    /**
     *
     */
    @PostConstruct
    public void init() {
        workhours = ejbFacade.findAll();
    }

    /**
     *
     * @param id
     * @return
     */
    public List<entities.Workhours> getForUser(Integer id) {
        ListIterator<entities.Workhours> li = workhours.listIterator();
        entities.Workhours w;
        
        List<entities.Workhours> wList;
        wList = new ArrayList<entities.Workhours>();
        
        while(li.hasNext()) {
            w = li.next();
            if(w.getUserid() == id) {
                wList.add(w);
            }
        }
        return wList;
    }

    
}
