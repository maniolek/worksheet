/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import javax.ejb.Stateful;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Mateusz Aniolek
 */
@Stateful
@ManagedBean(name = "authBean")
@SessionScoped
public class AuthBean implements Serializable {

    /**
     *
     */
    @PersistenceContext protected EntityManager entityManager;
    
    /**
     *
     */
    public String userScope = "USER";

    /**
     *
     */
    public String adminScope = "ADMIN";
    
    private boolean isAuth;
    
    private String login;
    
    private String password;

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    private String scope = "";

    /**
     *
     * @return
     */
    public String getScope() {
        return scope;
    }

    /**
     *
     * @param scope
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     *
     * @return
     */
    public boolean isAuth() {
        return isAuth;
    }

    /**
     *
     * @param isAuth
     */
    public void setAuth(boolean isAuth) {
        this.isAuth = isAuth;
    }

    /**
     *
     * @return
     */
    public String getLogin() {
        return login;
    }

    /**
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }
    
    /**
     *
     * @return
     */
    public String doLogin() {
    
        Query query = entityManager.createNativeQuery("SELECT * FROM users WHERE login = '" + this.login + "' AND password='" + this.password + "'");     
        
        if(query.getResultList().size() == 1) {
            this.isAuth = true;
            this.scope = "User";
            return "index.xhtml";
        } else {
            this.isAuth = false;
            this.scope = null;
            return "login.xhtml";
        }
        
    }
    
    /**
     *
     * @return
     */
    public String doLogout() {
        this.isAuth = false;
        this.scope = null;
        return "index.xhtml";
    }
    
}
