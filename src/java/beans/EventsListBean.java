/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.List;
import java.util.ListIterator;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Wojciech Rutkowski
 */
@Stateless
@ManagedBean (name = "eventsList")
@RequestScoped
public class EventsListBean {

    private String event;
    
    private List<entities.LoggedEvents> events;
    
    @EJB
    private jpa.beans.LoggedEventsFacade ejbFacade;
    
    /**
     *
     */
    @PostConstruct
    public void init() {
        events = ejbFacade.findAll();
    }

    /**
     *
     * @param id
     * @return
     */
    public entities.LoggedEvents getEvent(Integer id) {
        ListIterator<entities.LoggedEvents> li = events.listIterator();
        entities.LoggedEvents w;
        
        while(li.hasNext()) {
            w = li.next();
            if(w.getId().equals(id)) {
                return w;
            }
        }
        return null;
    }

    
}
