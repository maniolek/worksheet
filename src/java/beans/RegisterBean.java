/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Wojciech Rutkowski
 */
@Stateless
@ManagedBean(name = "registerBean")
public class RegisterBean extends entities.Users {

    /**
     *
     */
    @PersistenceContext protected EntityManager entityManager;
    
    private String repassword;

    /**
     *
     * @return
     */
    public String getRepassword() {
        return repassword;
    }

    /**
     *
     * @param repassword
     */
    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }
    
    /**
     *
     * @return
     */
    public String doRegister () {
        
        entityManager.persist(this);
        return "login.xhtml";
    }
    
}
