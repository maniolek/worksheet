/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entities.Users;
import java.util.List;
import java.util.ListIterator;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Wojciech Rutkowski
 */
@Stateless
@ManagedBean (name = "usersList")
@RequestScoped
public class UsersListBean {

    private String user;
    
    private List<entities.Users> users;
    
    @EJB
    private jpa.beans.UsersFacade ejbFacade;
    
    /**
     *
     */
    @PostConstruct
    public void init() {
        users = ejbFacade.findAll();
    }

    /**
     *
     * @return
     */
    public String getUser() {
        return user;
    }

    /**
     *
     * @param user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     *
     * @return
     */
    public List<Users> getUsers() {
        return users;
    }

    /**
     *
     * @param users
     */
    public void setUsers(List<Users> users) {
        this.users = users;
    }
    
    /**
     *
     * @param id
     * @return
     */
    public String getUser(Integer id) {
        ListIterator<entities.Users> li = users.listIterator();
        Users user;
        while(li.hasNext()) {
            user = li.next();
            if(user.getId().equals(id)) {
                return user.getLogin();
            }
        }
        return "-";
        
    }
    
}
