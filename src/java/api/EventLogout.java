/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import entities.LoggedEvents;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class EventLogout
 * Class for handling events, obtained by RFID/NFC devices. 
 * @author Mateusz Aniolek
 */
@WebServlet(name = "eventLogout")
public class EventLogout extends HttpServlet {
 
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        JsonGenerator generator = Json.createGenerator(out);
        response.setContentType("application/json");
        String param = request.getParameter("id");
        
        try {
            
            Integer id = Integer.valueOf(param);
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            
            DriverManager.registerDriver (new org.apache.derby.jdbc.ClientDriver()); 
            Connection conn = DriverManager.getConnection("jdbc:derby://localhost:1527/worksheet","app","app"); 
            Statement stmt = conn.createStatement(); 
            
            String query;
            
            query = "INSERT INTO LoggedEvents (eventtype, eventtimestamp, userid) VALUES ('" + LoggedEvents.EVENT_OUT + "', '" + ts + "', " + id + ")";
            stmt.executeUpdate(query);
                        
            query = "SELECT id, eventtype FROM LoggedEvents WHERE USERID = "+id+" ORDER BY id DESC";
            ResultSet rset = stmt.executeQuery(query);
            
            rset.next();
            
            Integer eventId = rset.getInt(1);
            String eventType = rset.getString(2);
            
            if(eventType.equals(LoggedEvents.EVENT_OUT)) {
                
                rset.next();
                String event2Type = rset.getString(2);
                if(event2Type.equals(LoggedEvents.EVENT_IN)) {
                    Integer neventId = rset.getInt(1);
                    query = "INSERT INTO Workhours (starteventid, endeventid, userid) VALUES (" + neventId + ", " + eventId + ", " + id + ")";
                    stmt.executeUpdate(query);
           
                }
                
            }
            
            generator.writeStartObject().write("result", "ok").writeEnd();
            
        } catch (Exception e) {
            
            generator.writeStartObject()
                    .write("result", "fail")
                    .write("message", e.getMessage())
                    .writeEnd();
            
        }
        generator.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
